import { Component, ViewChild,OnInit} from '@angular/core';
import {VisorImagComponent}from './visorImag/visorImag.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild(VisorImagComponent) private visor:VisorImagComponent;
  title = 'app';
  ngOnInit(){
this.mostrarImg();
  }
  mostrarImg(){
    let ELEMENTO= document.getElementById('IdImag');
    let srcImag=ELEMENTO.getAttribute('src');
    this.visor.mostrarImagen(srcImag);

  }
  zoomMas(){this.visor.ZoomIn();}
  zoomMenos(){this.visor.ZoomOut();}
  ZoomRestart(){this.visor.ZoomRefresh();}
  GirarMas(){this.visor.rotacionMas();}
  GirarMenos(){this.visor.rotacionMenos();}
}
