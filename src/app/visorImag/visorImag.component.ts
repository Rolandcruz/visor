import { Component, OnInit, AfterContentInit,Directive,Input, Output, HostListener, EventEmitter } from '@angular/core';
import 'fabric'
declare const fabric: any;

@Component({
  selector: 'app-visorImag',
  templateUrl: './visorImag.component.html',
  styleUrls: ['./visorImag.component.css']
})

@Directive({ selector: '[mouseWheel]' })

export class VisorImagComponent implements OnInit {

  
  constructor() { }

  imagenSelect:any;
  canvas : any;"";
   ngOnInit() {  
            this.cargarCanva();
              }








cargarCanva()
{
  let ventana=document.getElementById("conteinerMV");
  console.log('listo fabric v'+ fabric.version);  
  this.canvas= new fabric.Canvas('VisorFabric',{
    width:ventana.offsetWidth,
    height:ventana.offsetHeight,
    hoverCursor: 'pointer',
    selection: true,
    selectionBorderColor: 'blue',
    isDrawingMode:false,
    preserveObjectStacking:true,
    controlsAboveOverlay: true
  });
}
  mostrarImagen(base64:any)//solo necedita el url o el string de base 64
  {
    this.canvas.clear();
    let imagenUrl=base64;
    console.log('Manda la'+imagenUrl )
    //de aqui se ve una imagen desde la url pero hay que tener cuidado con la imagen
  fabric.Image.fromURL(imagenUrl,
    (image)=>{//objeto imagen depues sus componentes
            image.set({
              id:'imagenID',
              left: 0,
              top:0,
              angle: 0,
              hasControls :true,
              lockMovementX:false,
              lockMovementY:false,
              lockScalingX:true,
              lockScalingY:true,
              evented:true,
              selection: true,
              hasRotatingPoint:true,
              selectable  :true,
            });
        image.setWidth(image.getWidth()/2);
        image.setHeight(image.getHeight()/2);
        this.canvas.add(image); 
      });
      this.canvas.renderAll();
  }
  //el zoom se aplica a el canvas no a la imagen(espro que no haya problema)
  ZoomRefresh(){   
    this.canvas.setZoom(this.canvas.getZoom());
    this.canvas.renderAll();
  }
    ZoomOut(){
    this.canvas.setZoom(this.canvas.getZoom() / 1.1);
    this.canvas.renderAll();
    }
    ZoomIn(){
      this.canvas.setZoom(this.canvas.getZoom() * 1.1);
      this.canvas.renderAll();
    }
    rotacionMas(){
      let objto=this.canvas.getActiveObject();
      let AnguloAtive= objto.getAngle();
      let ne2=this.validarAngle(AnguloAtive,true);
      objto.setAngle(ne2);
      this.rotateImage(objto.get('id'));
      console.log("valor del doblaje"+ (ne2));
      this.canvas.renderAll();
    }

    rotacionMenos(){
      let objto=this.canvas.getActiveObject();
      let AnguloAtive= objto.getAngle();
      let ne2=this.validarAngle(AnguloAtive,false);
      objto.setAngle(ne2);
      this.rotateImage(objto.get('id'));
      console.log("valor del doblaje"+ (ne2));
      this.canvas.renderAll();
    } 

    rotateImage(id){
      let  colImg=this.canvas.getObjects("image");
        let pos=0,posY=0;
        //console.log(posY);
        this.canvas.renderAll();
        let angle;
        for(var p=pos;p<colImg.length;p++){	
          if(colImg[p].get("id")==id){
              //console.log(p+"-"+posY+" entro al if");
              angle=colImg[p].getAngle();
              if(angle==90 || angle==-270 ){
                colImg[p].setTop(0);
                colImg[p].setLeft(colImg[p].getHeight());
              }else if(angle==270 || angle == -90){
                colImg[p].setTop(colImg[p].getWidth());
                colImg[p].setLeft(0);
              }else if(Math.abs(angle)==180 ){
                colImg[p].setTop(colImg[p].getHeight());
                colImg[p].setLeft(colImg[p].getWidth());
              }
              else if (Math.abs(angle)==0){
                colImg[p].setTop(0);
                colImg[p].setLeft(0);
              }
              this.canvas.renderAll();
          }
            this.canvas.renderAll();
        }	
      }
    validarAngle(angle,signo){
      let newAngle:number;
      if(signo){
        newAngle=angle+90;
      }else{
        newAngle=angle-90;
      }
      if(newAngle>270 || newAngle<-270 ){
        return 0;
      }else{
        return newAngle;
      }
    }
    newImagen(){

	this.canvas.renderAll();
    }
//CHEck
@Output() mouseWheelUp = new EventEmitter();
@Output() mouseWheelDown = new EventEmitter();

    @HostListener('mousewheel', ['$event']) onMouseWheelChrome(event: any) {
      this.mouseWheelFunc(event);
    }

    @HostListener('DOMMouseScroll', ['$event']) onMouseWheelFirefox(event: any) {
      this.mouseWheelFunc(event);
    }

    @HostListener('onmousewheel', ['$event']) onMouseWheelIE(event: any) {
      this.mouseWheelFunc(event);
    }

    mouseWheelFunc(event: any) {
      var event = window.event || event; // old IE support
      var delta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));
      if(delta > 0) {
          this.ZoomIn();
      } else if(delta < 0) {
          this.ZoomOut();
      }
      // for IE
      event.returnValue = false;
      // for Chrome and Firefox
      if(event.preventDefault) {
          event.preventDefault();
      }
    }
}