import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {VisorImagComponent}from './visorImag/visorImag.component';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,VisorImagComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
